<?php

/**
 * @file
 * Page callbacks for overrides management.
 */

/**
 * Return a list of all the existing overrides numbers.
 */
function node_overrides_list($node) {
  $overrides = array();
  //$result = db_query('SELECT r.vid, r.title, r.log, r.uid, n.vid AS current_vid, r.timestamp, u.name FROM {node_revision} r LEFT JOIN {node} n ON n.vid = r.vid INNER JOIN {users} u ON u.uid = r.uid WHERE r.nid = :nid ORDER BY r.vid DESC', array(':nid' => $node->nid));
  $query = db_select('spaces_overrides', 'o');
  $query
    ->condition('o.object_type', 'node')
    ->condition('o.object_id', $node->nid)
    ->fields('o', array('id'));
  $results = $query->execute();
  foreach ($results as $result) {
    $space = spaces_load('og', $result->id);
    $overrides[$result->id] = node_load( $space->controllers->node->get($node->nid) );
  }

  return $overrides;
}

/**
 * Generate an overview table of older overrides of a node.
 */
function node_overrides_overview($node) {
  drupal_set_title(t('Overrides for %title', array('%title' => $node->title)), PASS_THROUGH);

  $header = array(t('Override'), array('data' => t('Operations'), 'colspan' => 2));

  $overrides = node_overrides_list($node);

  $rows = array();
  $revert_permission = FALSE;
  if ((user_access('revert revisions') || user_access('administer nodes')) && node_access('update', $node)) {
    $revert_permission = TRUE;
  }

  foreach ($overrides as $override) {
    $row = array();
    $operations = array();

    //$row[] = t('!date by !username', array('!date' => l(format_date($override->timestamp, 'short'), "node/$override->nid/view"), '!username' => theme('username', array('account' => $override))))
    //         . (($override->log != '') ? '<p class="override-log">' . filter_xss($override->log) . '</p>' : '');
    $row[] = t('!date by !username', array('!date' => l(format_date($override->created, 'short'), "node/$override->nid/view"), '!username' => theme('username', array('account' => $override))));
    $operations[] = l(t('diff'), "node/$node->nid/client-overrides/$override->nid/diff");
    if ($revert_permission) {
      $operations[] = l(t('revert'), "node/$node->nid/client-overrides/$override->nid/revert");
    }
    $rows[] = array_merge($row, $operations);
  }

  $build['node_overrides_table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return $build;
}

/**
 * Confirmation page for Override Revert.
 */
function node_override_revert_confirm($form, $form_state, $master, $override) {
  $form['#node'] = $override;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['nid'] = array('#type' => 'value', '#value' => $override->nid);
  $form['#master_nid'] = $master->nid;
  return confirm_form($form,
    t('Are you sure you want to revert the override "%title" back to master?', array('%title' => $override->title)),
    'node/' . $master->nid . '/client-overrides',
    t('This action cannot be undone.'),
    t('Revert'), t('Cancel'));
}

/**
 * Execute node deletion
 */
function node_override_revert_confirm_submit($form, &$form_state) {
  module_load_include('inc', 'node', 'node.pages');
  node_delete_confirm_submit($form, $form_state);
  $form_state['redirect'] = 'node/' . $form['#master_nid'] . '/client-overrides';
}


/**
 * Show a diff between master & override nodes.
 *
 * @param stdClass $node
 *   Original (master) node.
 * @param stdClass $override
 *   The space-override of the node
 */
function node_override_diff($node, $override) {
  module_load_include('inc', 'diff', 'diff.pages');

  // Generate table header (date, username, logmessage).
  $old_header = t('!date by !username', array(
      '!date' => l(format_date($node->revision_timestamp), "node/$node->nid/view"),
      '!username' => theme('username', array('account' => $node)),
  ));
  $new_header = t('!date by !username', array(
      '!date' => l(format_date($override->revision_timestamp), "node/$override->nid/view"),
      '!username' => theme('username', array('account' => $override)),
  ));

  $old_log = $node->log != '' ? '<p class="revision-log">'. filter_xss($node->log) .'</p>' : '';
  $new_log = $override->log != '' ? '<p class="revision-log">'. filter_xss($override->log) .'</p>' : '';

  // TODO implement next/previous diff links (see diff.pages.inc : diff_diffs_show() ).

  $cols = _diff_default_cols();
  $header = _diff_default_header($old_header, $new_header);
  $rows = array();
  if ($old_log || $new_log) {
    $rows[] = array(
        array(
            'data' => $old_log,
            'colspan' => 2
        ),
        array(
            'data' => $new_log,
            'colspan' => 2
        )
    );
  }
  $rows = array_merge($rows, _diff_body_rows($node, $override));
  $output = theme('diff_table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('diff')), 'cols' => $cols));

  $output .= '<div class="diff-section-title">'. t('Override:') .'</div>';
  // Don't include node links (final argument) when viewing the diff.
  $view = node_view($override, 'full');
  $output .= drupal_render($view);
  return $output;
}
